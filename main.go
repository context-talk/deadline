package main

import (
	"context"
	"fmt"
	"time"
)

func main() {
	deadline := time.Now().Add(time.Second)
	ctx, cancel := context.WithDeadline(context.Background(), deadline)
	defer cancel()

	nums := make(chan int)
	go func() {
		nums <- expensiveWorker()
	}()

	select {
	case num := <-nums:
		fmt.Printf("Got number! %d", num)
	case <-ctx.Done():
		fmt.Print(ctx.Err())
	}
}

func expensiveWorker() int {
	time.Sleep(5 * time.Second)

	return 10
}
